﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace peak_invest.Models
{
    public class Parcela
    {
        public int parcelas;
        public double valor;

        public double calcularValorTotal()
        {
            double valorTotal = parcelas * valor;
            valorTotal += valorTotal * .05; // 5%
            return valorTotal;
        }
    }
}