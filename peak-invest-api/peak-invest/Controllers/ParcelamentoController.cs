﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using peak_invest.Models;

namespace peak_invest.Controllers
{
    public class ParcelamentoController : ApiController
    {
        public string Post([FromBody]Parcela payload)
        {
            return string.Format("{0:C3}", payload.calcularValorTotal());
        }
    }
}
