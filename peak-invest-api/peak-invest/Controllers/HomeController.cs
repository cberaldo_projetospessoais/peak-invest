﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace peak_invest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Parcelamento()
        {
            return View();
        }

        public ActionResult Usuarios()
        {
            return View();
        }
    }
}
