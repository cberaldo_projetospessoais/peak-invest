﻿using peak_invest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace peak_invest.Controllers
{
    public class UsuarioController : ApiController
    {
        private List<KeyValuePair<int, string>> nomes;

        private UsuarioController()
        {
            nomes = new List<KeyValuePair<int, string>>();
            nomes.Add(new KeyValuePair<int, string>(1, "João"));
            nomes.Add(new KeyValuePair<int, string>(2, "Maria"));
            nomes.Add(new KeyValuePair<int, string>(3, "Ana"));
        }

        public IEnumerable<Usuario> Get(int id)
        {
            return from nome in nomes
                where nome.Key == id
                select new Usuario {
                    nome = nome.Value
                };
        }
    }
}
