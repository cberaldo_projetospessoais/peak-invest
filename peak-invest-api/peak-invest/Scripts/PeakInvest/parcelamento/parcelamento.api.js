﻿function ParcelamentoAPI() {
    this.url = '/api/Parcelamento';

    this.fetchValorParcelamento = (parcelamento) => {
        return fetch(this.url, {
            method: 'POST',
            body: JSON.stringify(parcelamento),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
    };
}
