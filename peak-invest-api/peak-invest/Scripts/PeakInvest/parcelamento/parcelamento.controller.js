﻿function ParcelamentoController($scope) {
    let api = new ParcelamentoAPI();

    $scope.parcelamento = {
        parcelas: undefined,
        valor: undefined,
    };
    $scope.Mensagem = "Informe o número de parcelas e o valor de cada uma para calcular o valor total.";

    $scope.calcularValorDoParcelamento = () => {
        api.fetchValorParcelamento($scope.parcelamento)
            .then(currency => {
                $scope.Mensagem = `O valor total do parcelamento é de ${currency}.`;
                $scope.$apply();
            });
    };
}
