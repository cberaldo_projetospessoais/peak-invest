﻿(function () {
    const app = angular.module('peak-invest-app', ['ngRoute']);

    app.controller('ParcelamentoController', ParcelamentoController);

    app.controller('UsuarioController', UsuarioController);
})();