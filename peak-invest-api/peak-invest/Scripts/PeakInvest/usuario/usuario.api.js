﻿
function UsuarioAPI() {
    this.url = '/api/Usuario';

    this.fetchNomeDoUsuarioPorId = (id) => {
        return fetch(`${this.url}/${id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(data => Promise.resolve(data.length > 0 ? data[0] : undefined))
    };

}
