﻿function UsuarioController($scope) {
    this.api = new UsuarioAPI();

    $scope.id = undefined;
    $scope.Mensagem = "Informe o id do usuário para consultar o nome.";

    $scope.buscarNomePorId = () => {
        this.api.fetchNomeDoUsuarioPorId($scope.id)
            .then(user => {
                $scope.Mensagem = !user ? `Usuário não encontrado` : `Usuário "${user.nome}" encontrado.`;
                $scope.$apply();
            });
    };
}